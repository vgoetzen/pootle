/**
 * i-doit
 *
 * "Monitoring" Module language file
 *
 * @package     monitoring
 * @subpackage  Language
 * @author      Leonard Fischer <lfischer@i-doit.com>
 * @copyright   2013 synetics GmbH
 * @version	    1.0.0
 * @license     http://www.i-doit.com/license
 */
return array(->'LC__CATG__MONITORING__INSTANCE'='Monitoring instance';
return array(->'LC__CATG__MONITORING__ACTIVE'='Aktiv?';
return array(->'LC__CATG__MONITORING__ALIAS'='Alias';
return array(->'LC__CATG__MONITORING__HOSTNAME'='Hostname';
return array(->'LC__CATG__MONITORING__HOSTNAME_SELECTION'='Hostname auswahl';
return array(->'LC__CATG__MONITORING__HOST'='Host';
return array(->'LC__CATG__LIVESTATUS'='Livestatus';
return array(->'LC__CATG__LIVESTATUS__CURRENT_STATE'='Aktueller status';
return array(->'LC__CATG__LIVESTATUS__HOST_STATE'='Host status';
return array(->'LC__CATG__LIVESTATUS__SERVICE_STATE'='Service status';
return array(->'LC__CATG__LIVESTATUS__NO_DATA'='Es wurden keine Daten von Livestatus empfangen.';
return array(->'LC__CATG__NDO'='NDO';
return array(->'LC__CATG__NDO__STATUS_CGI'='Status CGI';
return array(->'LC__MONITORING'='Monitoring';
return array(->'LC__MONITORING__LIVESTATUS'='Livestatus';
return array(->'LC__MONITORING__LIVESTATUS_STATUS'='Livestatus';
return array(->'LC__MONITORING__LIVESTATUS_STATUS_BUTTON'='Livestatus (Knopf)';
return array(->'LC__MONITORING__NDO'='NDO';
return array(->'LC__MONITORING__LIVESTATUS_NDO__CONFIGURATION'='Livestatus/NDO';
return array(->'LC__MONITORING__LIVESTATUS_NDO__CONFIGURATION_EDIT'='Livestatus/NDO Konfiguration bearbeiten';
return array(->'LC__MONITORING__EXPORT__CONFIGURATION'='Exportkonfiguration';
return array(->'LC__MONITORING__EXPORT__CONFIGURATION_EDIT'='Exportkonfiguration bearbeiten';
// Configuration labels.
return array(->'LC__MONITORING__EXPORT__PATH'='Export Verzeichnis';
return array(->'LC__MONITORING__ACTIVE'='Aktiv?';
return array(->'LC__MONITORING__TYPE'='Monitoring Typ';
return array(->'LC__MONITORING__CONNECTION'='Verbindungsart';
return array(->'LC__MONITORING__PATH'='Pfad';
return array(->'LC__MONITORING__ADDRESS'='Adresse';
return array(->'LC__MONITORING__PORT'='Port';
return array(->'LC__MONITORING__DBNAME'='Datenbankname / Schema';
return array(->'LC__MONITORING__DBPREFIX'='DB Prefix';
return array(->'LC__MONITORING__USERNAME'='Benutzer';
return array(->'LC__MONITORING__PASSWORD'='Passwort';
return array(->'LC__MONITORING__MONITORING_ADDRESS'='Link zum Monitoring Tool';
return array(->'LC__MONITORING__MONITORING_ADDRESS_INFO'='Achtung: Nagios benötigt den String "/status.cgi?host=" am ende';
return array(->'LC__MONITORING__EXPORT_PATH'='Lokaler Pfad';
return array(->'LC__MONITORING__EXPORT_PATH_WARNING'='Achtung: das angegebene Verzeichnis und dessen Inhalte werden vom System vor jedem Export geleert!';
return array(->'LC__MONITORING__NDO__STATUS'='NDO Status';
return array(->'LC__MONITORING__NDO__STATUS_BUTTON'='NDO Status Button';
// Exception translations.
return array(->'LC__MONITORING__LIVESTATUS_EXCEPTION__NO_CONFIG'='Bitte definieren Sie mindestens einen Monitoring Host.';
return array(->'LC__MONITORING__LIVESTATUS_EXCEPTION__PHP_EXTENSION_MISSING'='Es scheint, als würde die "sockets" PHP Extension fehlen!';
return array(->'LC__MONITORING__LIVESTATUS_EXCEPTION__COULD_NOT_CREATE_SOCKET'='Es konnte kein Socket zum Livestatus erstellt werden.';
return array(->'LC__MONITORING__LIVESTATUS_EXCEPTION__COULD_NOT_CONNECT_LIVESTATUS'='Es konnte keine Verbindung zum Livestatus hergestellt werden.';
return array(->'LC__MONITORING__LIVESTATUS_EXCEPTION__COULD_NOT_READ_FROM_SOCKET'='Beim lesen der Daten ist ein Fehler aufgetreten: %s';
return array(->'LC__MONITORING__LIVESTATUS_EXCEPTION__INVALID_FORMAT'='Das Abfrage-Ergebnis hat ein unbekanntes Format!';
return array(->'LC__MONITORING__NDO_EXCEPTION__NO_CONFIG'='Bitte definieren Sie mindestens einen NDO Host.';
