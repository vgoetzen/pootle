<?php
/**
 * i-doit
 *
 * "Custom fields" Module language file
 *
 * @package     custom fields
 * @subpackage  Language
 * @author      Van Quyen Hoang <qhoang@i-doit.com>
 * @copyright   2013 synetics GmbH
 * @version	    1.2.1
 * @license     http://www.i-doit.com/license
 */

return array(
	'LC__MODULE__JDISC__IMPORT__FILTER_TYPE__FILTER_HOSTADDRESS' => 'Filter nach Hostadresse(n)',
	'LC__MODULE__JDISC__IMPORT__FILTER_DEVICES_FOR_A_HOST_ADDRESS' => 'JDisc-Geräte nach einer Hostadresse filtern',
	'LC__MODULE__JDISC__IMPORT__FILTER_DEVICES_FOR_HOST_ADDRESSES_FROM_A_FILE' => 'JDisc-Geräte nach Hostadressen aus einer Datei filtern',
	'LC__MODULE__JDISC__OBJECT_TYPE_ASSIGNMENTS__PORT_FILTER' => 'Port Filter',
	'LC__MODULE__JDISC__OBJECT_TYPE_ASSIGNMENTS__PORT_FILTER__TYPE__NORMAL' => 'Normal Import',
	'LC__MODULE__JDISC__OBJECT_TYPE_ASSIGNMENTS__PORT_FILTER__TYPE__NO_IMPORT' => 'Kein Import',
	'LC__MODULE__JDISC__OBJECT_TYPE_ASSIGNMENTS__PORT_FILTER__TYPE__LOGICAL_PORT' => 'Logischer Port',
	'LC__MODULE__JDISC__OBJECT_TYPE_ASSIGNMENTS__PORT_FILTER__TYPE__PHYSICAL_PORT' => 'Physikalischer Port',
	'LC__MODULE__JDISC__IMPORT__FILTER_DEVICES_FOR_IP_FROM_FILE__DESCRIPTION' => 'In der Datei müssen alle Hostadressen untereinander stehen.',
	'LC__MODULE__JDISC__IMPORT__OVERWRITE_IP_ADDRESSES__DESCRIPTION_ACTIVATED' => 'Um diese Funktion verwenden zu können muss unter Systemeinstellungen der Unique check für IP-Adressen aktiviert sein. ',
	'LC__MODULE__JDISC__IMPORT__OVERWRITE_IP_ADDRESSES__DESCRIPTION_DEACTIVATED' => 'Diese Funktion verhindert das die importierten Objekte eine automatisch generierte IP-Adresse erhalten wenn es zu Konflikten mit anderen Geräten kommen sollte. Die in Konflikt geratenen IP-Adressen werden automatisch eines der Globalen Netzen zugewiesen, ohne dass sich die IP-Adresse verändert.',
	'LC__MODULE__JDISC__ADD_CUSTOM_ATTRIBUTES' => 'Custom attributes importieren',
	'LC__CMDB__CATG__JDISC_CUSTOM_ATTRIBUTES' => 'JDisc Custom Attributes',
	'LC__CATG__JDISC__CUSTOM_ATTRIBUTES__ATTRIBUTE' => 'Attribut',
	'LC__CATG__JDISC__CUSTOM_ATTRIBUTES__CONTENT' => 'Wert',
	'LC__CATG__JDISC__CUSTOM_ATTRIBUTES__FOLDER' => 'Ordner',
	'LC__CATG__JDISC__CUSTOM_ATTRIBUTES__TYPE' => 'Typ',
	'LC__CATG__JDISC__CUSTOM_ATTRIBUTES_TYPE__TEXT' => 'Text',
	'LC__CATG__JDISC__CUSTOM_ATTRIBUTES_TYPE__MULTITEXT' => 'Multiline text',
	'LC__CATG__JDISC__CUSTOM_ATTRIBUTES_TYPE__DATE' => 'Date',
	'LC__CATG__JDISC__CUSTOM_ATTRIBUTES_TYPE__TIME' => 'Time',
	'LC__CATG__JDISC__CUSTOM_ATTRIBUTES_TYPE__INTEGER' => 'Integer',
	'LC__CATG__JDISC__CUSTOM_ATTRIBUTES_TYPE__ENUMERATION' => 'Enumeration',
	'LC__CATG__JDISC__CUSTOM_ATTRIBUTES_TYPE__CURRENCY' => 'Currency',
	'LC__CATG__JDISC__CUSTOM_ATTRIBUTES_TYPE__DOCUMENT' => 'Document',
);