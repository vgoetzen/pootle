/**
 * i-doit
 *
 * "QR-Code" Module language file
 *
 * @package     qrcode
 * @subpackage  Language
 * @author      Leonard Fischer <lfischer@i-doit.com>
 * @copyright   2013 synetics GmbH
 * @version	    1.0.0
 * @license     http://www.i-doit.com/license
 */
return array(->'LC__MODULE__QRCODE'='QR-Code';
return array(->'LC__MODULE__QRCODE__CONFIGURATION'='Globale QR-Code Konfiguration';
return array(->'LC__MODULE__QRCODE__CONFIGURATION__BY_OBJ_TYPE'='QR-Code Konfiguration nach Objekt-Typ';
return array(->'LC__MODULE__QRCODE__CONFIGURATION__QR_METHOD'='Methode zur Generierung des QR-Codes';
return array(->'LC__MODULE__QRCODE__CONFIGURATION__GLOBAL_CONFIGURATION'='Die globale Konfiguration bezieht sich auf alle Objekte und Objekt-Typen, die nicht explizit eigene Konfigurationen mitbringen.';
return array(->'LC__MODULE__QRCODE__CONFIGURATION__PRIMARY_URL'='Primäre Zugriffs-URL';
return array(->'LC__MODULE__QRCODE__CONFIGURATION__PRIMARY_URL_DESCRIPTION'='QR-Code wird anhand der Primären Zugriffs-URL aus der Kategorie Zugriff generiert';
return array(->'LC__MODULE__QRCODE__CONFIGURATION__DESCRIPTION'='Textfeld für zusätzliche Informationen';
return array(->'LC__MODULE__QRCODE__CONFIGURATION__GLOBAL_DEFINITION'='Globale Definition';
return array(->'LC__MODULE__QRCODE__CONFIGURATION__LOGO'='Logo für Druckansicht';
return array(->'LC__MODULE__QRCODE__CONFIGURATION__NEW_CONFIG'='Neue Konfiguration';
// Translations for the auth GUI.
return array(->'LC__AUTH_GUI__BLABLA'='Tags konfigurieren';
return array(->'LC__AUTH__QRCODE_EXCEPTION__BLABLA'='Es ist Ihnen nicht erlaubt, die QRCODE Tag konfiguration öffnen.';
// Translations for the report view.
return array(->'LC__REPORT__VIEW__QR_CODES'='QR Codes';
return array(->'LC__REPORT__VIEW__QR_CODES_DESCRIPTION'='Zeigt mehrere QR Codes von mehreren Objekten in einer Druckbaren Ansicht an.';
return array(->'LC__REPORT__VIEW__QR_CODES_SELECT_OBJECTS'='Bitte wählen Sie die Objekte aus, deren QR Code Sie dargestellt bekommen möchten.';
return array(->'LC__REPORT__VIEW__QR_CODES_SIZE'='Größe des QR Codes';
return array(->'LC__REPORT__VIEW__QR_CODES_COLUMNS'='Anzahl der Spalten';
return array(->'LC__REPORT__VIEW__QR_CODES_POPUP'='Ergebnis im Popup öffnen';
return array(->'LC__REPORT__VIEW__QR_CODES__NO_URL_MESSAGE'='Achtung! Für das Objekt %s konnte keine URL generiert werden.';
return array(->'LC__REPORT__VIEW__QR_CODES__CORRECTION'='Fehlerkorrektur';
return array(->'LC__REPORT__VIEW__QR_CODES__CORRECTION__LOW'='Niedrig';
return array(->'LC__REPORT__VIEW__QR_CODES__CORRECTION__MEDIUM'='Normal';
return array(->'LC__REPORT__VIEW__QR_CODES__CORRECTION__QUALITY'='Gut';
return array(->'LC__REPORT__VIEW__QR_CODES__CORRECTION__HIGH'='Hoch';
return array(->'LC__REPORT__VIEW__QR_CODES__LAYOUT_SELECTION'='Layout auswählen';
return array(->'LC__REPORT__VIEW__QR_CODES__LAYOUT_QRCODE'='QR Code';
return array(->'LC__REPORT__VIEW__QR_CODES__LAYOUT_DESCRIPTION'='Beschreibung';
