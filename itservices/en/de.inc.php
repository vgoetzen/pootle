/**
 * i-doit
 *
 * "IT-Service" Module language file.
 *
 * @package     modules
 * @subpackage  itservice
 * @author      Leonard Fischer <lfischer@i-doit.com>
 * @version     1.0.0
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 * @since       i-doit 1.4
 */
return array(->'LC__MODULE__ITSERVICE'='IT-Services';
return array(->'LC__ITSERVICE__CONFIG'='Service Filter';
return array(->'LC__ITSERVICE__CONFIG__TITLE'='Filter-name';
return array(->'LC__ITSERVICE__CONFIG__ACTIVE'='Aktiv';
return array(->'LC__ITSERVICE__CONFIG__CONDITION'='Bedingung';
return array(->'LC__ITSERVICE__CONFIG__PARAMETER'='Parameter';
return array(->'LC__ITSERVICE__CONFIG__WARNING__NO_CONFIGURATION_SELECTED'='Es wurde keine Konfiguration ausgewählt!';
return array(->'LC__ITSERVICE__CONFIG__SUCCESS__CONFIGURATION_PURGED'='Die ausgewählten Konfigurationen wurden gelöscht!';
return array(->'LC__ITSERVICE__CONFIG_FILTER__RELATIONTYPE'='Beziehungstyp';
return array(->'LC__ITSERVICE__CONFIG_FILTER__RELATIONTYPE_CONDITION'='stimmt nicht überein mit';
return array(->'LC__ITSERVICE__CONFIG_FILTER__PRIORITY'='Priorität / Gewichtung';
return array(->'LC__ITSERVICE__CONFIG_FILTER__PRIORITY_CONDITION'='liegt nicht unterhalb von';
return array(->'LC__ITSERVICE__CONFIG_FILTER__OBJTYPE'='Objekttyp';
return array(->'LC__ITSERVICE__CONFIG_FILTER__OBJTYPE_CONDITION'='stimmt nicht überein mit';
return array(->'LC__ITSERVICE__CONFIG_FILTER__LEVEL'='Ebene';
return array(->'LC__ITSERVICE__CONFIG_FILTER__LEVEL_CONDITION'='bis zu einer tiefe von';
return array(->'LC__ITSERVICE__TYPES'='Services';
return array(->'LC__ITSERVICE__TYPE_CONFIGURATION'='Service Typ Konfiguration';
return array(->'LC__ITSERVICE__TYPE_CONFIGURATION__TITLE'='Typ Titel';
return array(->'LC__ITSERVICE__TYPE_CONFIGURATION__DESCRIPTION'='Typ Beschreibung';
