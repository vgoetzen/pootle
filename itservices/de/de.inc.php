<?php
/**
 * i-doit
 *
 * "IT-Service" Module language file.
 *
 * @package     modules
 * @subpackage  itservice
 * @author      Leonard Fischer <lfischer@i-doit.com>
 * @version     1.0.0
 * @copyright   synetics GmbH
 * @license     http://www.i-doit.com/license
 * @since       i-doit 1.4
 */

return array(
	'LC__MODULE__ITSERVICE' => 'IT-Services',

	'LC__ITSERVICE__CONFIG' => 'Service Filter',
	'LC__ITSERVICE__CONFIG__TITLE' => 'Filter-name',
	'LC__ITSERVICE__CONFIG__ACTIVE' => 'Aktiv',
	'LC__ITSERVICE__CONFIG__CONDITION' => 'Bedingung',
	'LC__ITSERVICE__CONFIG__PARAMETER' => 'Parameter',
	'LC__ITSERVICE__CONFIG__WARNING__NO_CONFIGURATION_SELECTED' => 'Es wurde keine Konfiguration ausgewählt!',
	'LC__ITSERVICE__CONFIG__SUCCESS__CONFIGURATION_PURGED' => 'Die ausgewählten Konfigurationen wurden gelöscht!',
	'LC__ITSERVICE__CONFIG_FILTER__RELATIONTYPE' => 'Beziehungstyp',
	'LC__ITSERVICE__CONFIG_FILTER__RELATIONTYPE_CONDITION' => 'stimmt nicht überein mit',
	'LC__ITSERVICE__CONFIG_FILTER__PRIORITY' => 'Priorität / Gewichtung',
	'LC__ITSERVICE__CONFIG_FILTER__PRIORITY_CONDITION' => 'liegt nicht unterhalb von',
	'LC__ITSERVICE__CONFIG_FILTER__OBJTYPE' => 'Objekttyp',
	'LC__ITSERVICE__CONFIG_FILTER__OBJTYPE_CONDITION' => 'stimmt nicht überein mit',
	'LC__ITSERVICE__CONFIG_FILTER__LEVEL' => 'Ebene',
	'LC__ITSERVICE__CONFIG_FILTER__LEVEL_CONDITION' => 'bis zu einer tiefe von',

	'LC__ITSERVICE__TYPES' => 'Services',
	'LC__ITSERVICE__TYPE_CONFIGURATION' => 'Service Typ Konfiguration',
	'LC__ITSERVICE__TYPE_CONFIGURATION__TITLE' => 'Typ Titel',
	'LC__ITSERVICE__TYPE_CONFIGURATION__DESCRIPTION' => 'Typ Beschreibung',
);